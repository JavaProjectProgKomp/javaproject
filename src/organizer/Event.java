package organizer;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * Klasa wydarzen.
 */
public class Event {
	/**
	 * Data wydarzenia.
	 */
	private LocalDate date;
	/**
	 * Czas wydarzenia.
	 */
	private LocalTime time;
	/**
	 * Tytu� wydarzenia.
	 */
	private String title;
	/**
	 * Opis wydarzenia.
	 */
	private String note;
	/**
	 * Dzien alarmu wydarzenia.
	 */
	private LocalDate alarmDate;
	/**
	 * Czas alarmu wydarzenia.
	 */
	private LocalTime alarmTime;


	/**
	 * Konstruktor ustawiaj�cy dat� podan� w argumencie.
	 * @param ld - data wydarzenia.
	 */
	public Event(LocalDate ld){
		this.date=ld;
	}
	/**
	 * Konstruktor ustawiaj�cy dat� oraz czas wydarzenia.
	 * @param ld - data wydarzenia.
	 * @param lt - czas wydarzenia.
	 */
	public Event(LocalDate ld, LocalTime lt){
		this.date=ld;
		this.time=lt;
	}
	/**
	 * Konstruktor ustawiaj�cy dat�, czas, tytu� oraz opis wydarzenia.
	 * @param ld - data wydarzenia.
	 * @param lt - czas wydarzenia.
	 * @param t - tytu� wydarzenia.
	 * @param n - opis wydarzenia.
	 */
	public Event(LocalDate ld, LocalTime lt, String t, String n){
		this.date = ld;
		this.time = lt;
		this.title = t;
		this.note = n;
	}
	/**
	 * Konstruktor ustawiaj�cy dat�, czas, tytu�, opis, dat� oraz czas alarmu wydarzenia.
	 * @param ld - data wydarzenia.
	 * @param lt - czas wydarzenia.
	 * @param t - tytu� wydarzenia.
	 * @param n - opis wydarzenia.
	 * @param alarmDate - dat� alarmu wydarzenia.
	 * @param alarmTime - czas alarmu wydarzenia.
	 */
	public Event(LocalDate ld, LocalTime lt, String t, String n, LocalDate alarmDate, LocalTime alarmTime){
		this.date = ld;
		this.time = lt;
		this.title = t;
		this.note = n;
		this.alarmDate = alarmDate;
		this.alarmTime = alarmTime;
	}


	@Override
	/**
	 * Metoda zwraca String z�o�ony z daty, czasu, opisu, wydarzenia, a tak�e (j�eli jest ustawiony) date oraz czas alarmu.
	 */
	public String toString(){
		if(alarmDate!=null && alarmTime!=null){
			return date.toString() + " " + time.toString()+"\n"+title+"\n"+note+"\n"+alarmDate.toString()+" "+alarmTime.toString();
		}
		else{
			return date.toString() + " " + time.toString()+"\n"+title+"\n"+note;
		}

	}

	/**
	 * Zwraca dat� wydarzenia.
	 * @return dat� wydarzenia.
	 */
	public LocalDate getDate(){
		return this.date;
	}
	/**
	 * Zwraca czas wydarzenia.
	 * @return czas wydarzenia.
	 */
	public LocalTime getTime(){
		return this.time;
	}
	/**
	 * Zwraca tytu� wydarzenia.
	 * @return tytu� wydarzenia.
	 */
	public String getTitle(){
		return this.title;
	}
	/**
	 * Zwraca opis wydarzenia.
	 * @return opis wydarzenia.
	 */
	public String getNote(){
		return this.note;
	}
	/**
	 * Zwraca dat� alarmu wydarzenia.
	 * @return dat� alarmu wydarzenia.
	 */
	public LocalDate getAlarmDate(){
		return this.alarmDate;
	}
	/**
	 * Zwraca czas alarmu wydarzenia.
	 * @return czas alarmu wydarzenia.
	 */
	public LocalTime getAlarmTime(){
		return this.alarmTime;
	}


	/**
	 * Ustawia dat� wydarzenia.
	 * @param ld data wydarzenia.
	 */
	public void setDate(LocalDate ld){
		this.date = ld;
	}
	/**
	 * Ustawia czas wydarzenia.
	 * @param lt czas wydarzenia.
	 */
	public void setTime(LocalTime lt){
		this.time = lt;
	}
	/**
	 * Ustawia tytu� wydarzenia.
	 * @param t tytu� wydarzenia.
	 */
	public void setTitle(String t){
		this.title=t;
	}
	/**
	 * Ustawia opis wydarzenia.
	 * @param n opis wydarzenia.
	 */
	public void setNote(String n){
		this.note=n;
	}
	/**
	 * Ustawia dat� alarmu wydarzenia.
	 * @param ld data alarmu wydarzenia.
	 */
	public void setAlarmDate(LocalDate ld){
		this.alarmDate=ld;
	}
	/**
	 * Ustawia czas alarmu wydarzenia.
	 * @param lt czas alarmu wydarzenia.
	 */
	public void setAlarmTime(LocalTime lt){
		this.alarmTime=lt;
	}



}
