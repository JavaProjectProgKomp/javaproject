package organizer;

import java.awt.*;
import java.time.LocalDate;
import java.time.LocalTime;
import javax.swing.*;

import model.MainFrameModel;
import view.MainFrameView;

//import com.sun.prism.paint.Color;

/**
 * Klasa "MAIN".
 */
public class MainClass{
	/**
	 * Funkcja MAIN
	 * @param args
	 */
	public static void main(String[] args){


		EventQueue.invokeLater(new Runnable(){

			@Override
			public void run() {
				MainFrameModel mfm = new MainFrameModel();
				final MainFrameView frame = new MainFrameView(mfm);
				frame.setTitle("1UP!");

				Runnable r = new Runnable() {
					@Override
					public void run() {


						for(;;){

							for(int i = 0; i!=EventsList.getArray().size();i++){
								if( EventsList.getArray().get(i).getAlarmDate()!=null && EventsList.getArray().get(i).getAlarmTime()!=null){
									//System.out.println(i);
									if( LocalDate.now().isEqual( EventsList.getArray().get(i).getAlarmDate() )
											&& LocalTime.now().isAfter( EventsList.getArray().get(i).getAlarmTime() )  ){
										while(true){


											String message = EventsList.getArray().get(i).getTitle() + " \n"+
													EventsList.getArray().get(i).getDate() + " " +
													EventsList.getArray().get(i).getTime() + "\n" +
													EventsList.getArray().get(i).getNote();

											Toolkit.getDefaultToolkit().beep();
											Object[] options = {"OK"};
											int alarm = JOptionPane.showOptionDialog(frame,
													message,"Alarm",
													JOptionPane.PLAIN_MESSAGE,
													JOptionPane.QUESTION_MESSAGE,
													null,
													options,
													options[0]);


											if(alarm == JOptionPane.OK_OPTION){
												EventsList.getArray().get(i).setAlarmDate(null);
												EventsList.getArray().get(i).setAlarmTime(null);
												break;
											}
										}
									}
								}
							}
							try {
								Thread.sleep(100);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}

					}
				};


				new Thread(r).start();

			}
		});
	}
}







