package organizer;

import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;


/**
 * Klasa rozwijanej listy wyboru daty
 *
 */
public class DateChoise extends JComboBox {

	/**
	 * Lista Stringow rocznik�w.
	 */
	private ArrayList<String> yearsList;

	/**
	 * Konstruktor domy�lny.
	 *
	 * Inicjalizuje yearsList lista 10 lat w prz�d, zaczynaj�c od roku bierz�cego.
	 */
	public DateChoise(){

		this.yearsList = new ArrayList<String>();
		for(int years = Calendar.getInstance().get(Calendar.YEAR) ; years<=Calendar.getInstance().get(Calendar.YEAR)+10;years++){
			yearsList.add(years+"");

			DefaultComboBoxModel lata = new DefaultComboBoxModel(yearsList.toArray());
			super.setModel(lata);

		}

	}

}
