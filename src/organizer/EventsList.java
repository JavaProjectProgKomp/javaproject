package organizer;

import java.util.ArrayList;



//	Klasa GLOBALNA przechowujaca wydarzenia
/**
 * Klasa kontenerowa przechowuj�ca wszystkie wydarzenia
 *
 */
public final class EventsList extends ArrayList {

	/**
	 * ArrayList przechowuj�cy wszystkie wydarzenia.
	 */
	private static ArrayList<Event> eventsList = new ArrayList<Event>();

	/**
	 * Zwraca indeks wydarzenia, kt�re posiada taki sam tytu�, jak argument metody.
	 * @param title tytu� wzorcowy
	 * @return indeks elemntu posiadajacego taki sam tytu�. W prypadku braku takowego zwraca -1.
	 */
	public static int getEventIndex(String title){
		for(int i = 0; i!=EventsList.eventsList.size(); i++){
			if(eventsList.get(i).getTitle()==title){
				return i;
			}
		}
		return -1;
	}

	/**
	 * Zwraca liste wydarze�.
	 * @return liste wydarze�.
	 */
	public static ArrayList<Event> getArray(){
		return EventsList.eventsList;
	}
	/**
	 * Ustawia liste wydarze�.
	 * @param al listaw wydarze�.
	 */
	public static void setArray(ArrayList<Event> al){
		EventsList.eventsList=al;
	}

}
