package view;

import model.DayButtonModel;
import model.MainFrameModel;
import organizer.DateChoise;
import view.DayButtonView;
import organizer.EventsList;
import view.MenuView;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import controller.MainFrameController;




/**
 *
 * Klasa widoku g��wnego okna aplikacji.
 */
public class MainFrameView extends JFrame {

	/**
	 * Model g��wnego okna aplikacji.
	 */
	private MainFrameModel model;
	/**
	 * G�rny panel okna z ustawionym FlowLayout.
	 */
	private JPanel panelGora = new JPanel(new FlowLayout());
	/**
	 * �rodkowy panel okna z ustawionym GridLayout.
	 */
	private JPanel panelCenter = new JPanel(new GridLayout(7,7,10,10));
	/**
	 * Lewy panel okna z ustawionym FlowLayout.
	 */
	private JPanel panelLewy = new JPanel(new FlowLayout());
	/**
	 * Prawy panel okna z ustawionym FlowLayout.
	 */
	private JPanel panelPrawy = new JPanel(new FlowLayout());
	/**
	 * Etykieta "Rok".
	 */
	private JLabel rok = new JLabel("Rok: ",10);
	/**
	 * Etykieta "Miesi�c".
	 */
	private JLabel miesiac = new JLabel("Miesiac: ",10);
	/**
	 * Pasek menu okna aplikacji.
	 */
	private MenuView menuBar;
	/**
	 * Rozwijana lista wyboru miesi�ca.
	 */
	final private JComboBox wyborMiesiaca;
	/**
	 * Rozwijana lista wyboru roku.
	 */
	final private DateChoise wyborRoku;

	/**
	 * Konstruktor tworz�cy g��wne okno aplikacji i inicjalizuj�cy zmienn� modelu tego okna.
	 * @param mfm model g��wnego okna aplikacji.
	 */
	public MainFrameView(MainFrameModel mfm){
		this.model=mfm;

		menuBar = new MenuView(this);
		int changes=EventsList.getArray().size();
		if(changes!=EventsList.getArray().size())
			this.refreshView();

		loadConfig();





		String[] miesiace = model.getStringMiesiace();
		wyborMiesiaca = new JComboBox<Object>(miesiace);
		wyborMiesiaca.setSelectedIndex(LocalDate.now().getMonthValue());
		wyborRoku = new DateChoise();

		//		ustawienie layoutu okna glownego
		setLayout(new BorderLayout());

		//	ustawia rozmiar okna i wycentrowanie go
		setSize(600,600);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		//	ustawia ikonke
		Image ico = new ImageIcon("icon.png").getImage();
		setIconImage(ico);
		setVisible(true);

		//		dodanie komponentow do gornego panelu
		//	(comboboxy miesiaca i roku wraz z opisem)
		panelGora.add(miesiac);

		panelGora.add(wyborMiesiaca);
		panelGora.add(rok);
		panelGora.add(wyborRoku);
		panelGora.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		add(panelGora,BorderLayout.NORTH);	//	dodanie panelu do polnocnej czesci layoutu

		//		ustawienia nasluchiwacza COMBOBOX z wyborem miesiaca
		wyborMiesiaca.addActionListener(new MainFrameController(model,this).getMonthAction());
		wyborMiesiaca.setOpaque(true);

		// 		ustawienia nasluchiwacza COMBOBOX z wyborem roku
		wyborRoku.addActionListener (new MainFrameController(model,this).getYearAction());
		wyborRoku.setOpaque(true);

		add(panelCenter,BorderLayout.CENTER);
		panelLewy.add(new JLabel("    		 	"));
		add(panelLewy,BorderLayout.WEST);
		panelPrawy.add(new JLabel("       	  	"));
		add(panelPrawy,BorderLayout.EAST);

		setJMenuBar(menuBar);

		rysujMiesiac();


	}
	/**
	 * Metoda zwraca model g��wnego okna aplikacji.
	 * @return model g��wnego okna aplikacji.
	 */
	public MainFrameModel getModel(){
		return this.model;
	}
	/**
	 * Metoda zwraca g�rny panel.
	 * @return g�rny panel.
	 */
	public JPanel getPanelGora(){
		return this.panelGora;
	}
	/**
	 * Metoda zwraca �rodkowy panel.
	 * @return �rodkowy panel.
	 */
	public JPanel getPanelCenter(){
		return this.panelCenter;
	}
	/**
	 * Metoda zwraca rozwijan� list� wyboru miesi�ca.
	 * @return rozwijan� list� wyboru miesi�ca.
	 */
	public JComboBox getWyborMiesiaca(){
		return this.wyborMiesiaca;
	}
	/**
	 * Metoda zwraca rozwijan� liste wyboru roku.
	 * @return rozwijan� liste wyboru roku.
	 */
	public JComboBox getWyborRoku(){
		return this.getWyborRoku();
	}
	/**
	 * Metoda zwraca wybrany rok z listy w postaci obiektu z pomoc� metody JComboBox.getSeectedItem();.
	 * @return wybrany rok jako Object.
	 */
	public Object getSelectedYear(){
		return wyborRoku.getSelectedItem();
	}
	/**
	 * Metoda dodaje przyciski dni do �rodkowego panelu g��wnego okna aplikacji, kt�re po klikni�ciu wy�wietlaj� list� wydarze� danego dnia.
	 */
	public void rysujMiesiac(){

		//	czyszczenie listy przyciskow
		if(!model.getButtonList().isEmpty()){
			panelCenter.removeAll();
			model.getButtonList().removeAll(model.getButtonList());
			validate();
			repaint();
		}

		for(int i = 0; i!=7; i++){
			panelCenter.add(new JLabel(model.getStringDni()[i]));
			refreshView();
		}

		LocalDate data = LocalDate.now();
		data = data.withDayOfMonth(1);
		data = data.withMonth(model.getNrMiesiaca());
		data = data.withYear(model.getnrRoku());
		LocalDate koniecMiesiaca = data.withDayOfMonth(data.lengthOfMonth());
		int przes = 0;
		int rok = data.getYear();
		while(przes!=data.getDayOfWeek().getValue()){
			przes++;
		}
		//		przyciski odpowiadajace za dzien
		int x = 0;
		for (int i = 1;i!=7;i++){
			for(int ii = 1; ii!=8; ii++){
				if(i==1 && ii<przes){
					panelCenter.add(new JLabel(""));

					refreshView();
				}
				//	Wstawia puste miejsce w na poczatku kalendarza, jezeli nie ma takiego dnia miesiaca w tym tygodniu
				else if( (data.getDayOfMonth()+x) == (koniecMiesiaca.getDayOfMonth()+1) ){
					panelCenter.add(new JLabel(""));
					refreshView();
				}
				else{
					// Do widoku dodawany jest nowy przycisk z przekazanymi danymi odnosnie konkretnego dnia
					//	oraz widok okna glownego (this), aby byla mozliwosc odsiezenia go, po wprowadzeniu zmian
					//	w liscie wydarzen (dodanie wydarzenia)
					//	Od tej pory widok ramki glownej przekazywany jest z klasy do klasy aby zachowac mozliwosc
					//	odswiezenia jej po danej akcji
					//	MainFrameView -> DayButton -> EventFrameView -> NewEventFrameView
					//  Po dodaniu nowego Eventu nastepuje odswiezenie
					model.getButtonList().add(new DayButtonView(new DayButtonModel(x+1,model.getNrMiesiaca(),rok),this));
					model.getButtonList().get(x).setBackground(Color.lightGray);
					panelCenter.add(model.getButtonList().get(x));

					refreshView();

					//	W przypadku niedzieli, koloruje przycisk na czerwono
					if(ii==7){
						model.getButtonList().get(x).setBackground(Color.RED);

						this.refreshView();
					}
					//	Ustawia t�o przycisku w ptrzypadku, gdy w danym dniu sa ustawione jakies wydarzenia
					if(!EventsList.getArray().isEmpty()){
						for(int j=0; j!=EventsList.getArray().size(); j++){
							if(EventsList.getArray().get(j).getDate().getDayOfMonth() == model.getButtonList().get(x).getDayButtonModel().getNrDnia() &&
									EventsList.getArray().get(j).getDate().getMonthValue() == model.getButtonList().get(x).getDayButtonModel().getNrMiesiaca() &&
									EventsList.getArray().get(j).getDate().getYear() == model.getButtonList().get(x).getDayButtonModel().getNrRoku() ){
								model.getButtonList().get(x).setBackground(Color.GREEN);

								refreshView();
							}
						}
					}else if(ii!=7){
						model.getButtonList().get(x).setBackground(Color.LIGHT_GRAY);
					}

					x++;
				}
			}
		}
	}
	/**
	 * Metoda o�wie�a widok okna.
	 */
	public void refreshView(){
		//this.revalidate();
		this.validate();
		this.repaint();
	}
	/**
	 * Metoda ustawia motym graficzny aplikacji o podanej nazwie.
	 * @param themeName nazwa motywu graficznego.
	 */
	public void setTheme(String themeName){

		try {

			if(themeName.equals("Nimbus")){

				UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
			}


			else if (themeName.equals("System")) {
				UIManager.getSystemLookAndFeelClassName();
			}

			else if (themeName.equals("Motif")) {
				UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
			}

			else if (themeName.equals("Windows")) {
				UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");


			}
			else if (themeName.equals("Metal")) {
				UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");

			}


		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	/**
	 * Metoda wczytuje ustawienia aplikacji z pliku config.cfg.
	 */
	public void loadConfig(){

		try {
			String theme;
			BufferedReader br = new BufferedReader(new FileReader(new File("config.cfg")));
			theme = br.readLine();
			setTheme(theme);

			//System.out.println(theme);

			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}


// ############################################################################################




//############################################################################################


