package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTabbedPane;

import controller.SettingsFrameController;
import model.SettingsFrameModel;


/**
 * Klasa widoku okna ustawie� aplikacji.
 */
public class SettingsFrameView extends JFrame{

	/**
	 * Model okna ustawie�.
	 */
	private SettingsFrameModel model = new SettingsFrameModel();
	/**
	 * Panel zak�adkowy.
	 */
	private JTabbedPane tabs;
	/**
	 * Panel 1szej zak�adki.
	 */
	private JPanel panel1;
	/**
	 * Panel 2giej zak�adki.
	 */
	private JPanel panel2;

	/**
	 * Konstruktor domy�lny widoku okna ustawie�.
	 * Definiuje rozmiary okna, dodaje oraz grupuje JRadioButtony do pierwszej zak�adki, za pomoc� kt�rych wybieramy motyw graficzny aplikacji.
	 */
	public SettingsFrameView(){

		super("Preferences");

		tabs = new JTabbedPane();
		panel1 = new JPanel(new BorderLayout());
		panel2 = new JPanel();

		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setSize(300,300);
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);

		JRadioButton rb1 = new JRadioButton("Nimbus");
		JRadioButton rb2 = new JRadioButton("System");
		JRadioButton rb3 = new JRadioButton("Motif");
		JRadioButton rb4 = new JRadioButton("Windows");
		JRadioButton rb5 = new JRadioButton("Metal");

		rb1.setSelected(true);

		rb1.addActionListener(new SettingsFrameController(this).getRadioButtonsAction());
		rb2.addActionListener(new SettingsFrameController(this).getRadioButtonsAction());
		rb3.addActionListener(new SettingsFrameController(this).getRadioButtonsAction());
		rb4.addActionListener(new SettingsFrameController(this).getRadioButtonsAction());
		rb5.addActionListener(new SettingsFrameController(this).getRadioButtonsAction());

		ButtonGroup group = new ButtonGroup();
		group.add(rb1);
		group.add(rb2);
		group.add(rb3);
		group.add(rb4);
		group.add(rb5);

		Box sizeBox = Box.createVerticalBox();
		sizeBox.add(rb1);
		sizeBox.add(rb2);
		sizeBox.add(rb3);
		sizeBox.add(rb4);
		sizeBox.add(rb5);
		sizeBox.setBorder(BorderFactory.createTitledBorder("Choose theme"));

		panel1.add(sizeBox,BorderLayout.CENTER);

		JPanel buttonsPanel = new JPanel(new GridLayout());
		JButton ok = new JButton("OK");
		ok.addActionListener(new SettingsFrameController(this).getOkButtonAction());
		buttonsPanel.add(ok);


		buttonsPanel.add(new JButton("Cancel"));

		panel1.add(buttonsPanel,BorderLayout.SOUTH);

		tabs.addTab("Themes", panel1);

		tabs.addTab("Loading", panel2);

		add(tabs);
		setSize(300, 300);
		setVisible(true);

		System.out.println("!");

	}
	/**
	 *Metoda zwraca model okna ustawie�.
	 * @return model okna ustawie�.
	 */
	public SettingsFrameModel getModel(){
		return model;
	}
}


