package view;

import java.awt.event.InputEvent;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import com.sun.glass.events.KeyEvent;
import controller.MenuController;
import controller.SettingsFrameController;

/**
 * Klasa menu organizera.
 */
public class MenuView extends JMenuBar {

	/**
	 * Zmienna widoku g��wnego okna aplikacji.
	 */
	MainFrameView mfv;
	/**
	 * Konstruktor dodaj�cy pasek menu do aplikacji wraz z jego wszystkimi elementami.
	 * @param mfv - widok g��wnego okna aplikacji.
	 */
	public MenuView(MainFrameView mfv){

		this.mfv=mfv;

		// 	#	Menu FILE
		JMenu fileMenu = new JMenu("File");
		add(fileMenu);


		// 	##	Podmenu SAVE
		JMenu saveMenu = new JMenu("Save to");
		fileMenu.add(saveMenu);

		JMenuItem saveToDatabase = new JMenuItem("Database");
		saveMenu.add(saveToDatabase);
		saveToDatabase.addActionListener(new MenuController(mfv).getSaveSqlAction());
		// Skr�t Ctrl+D zapisujacy DB
		KeyStroke controlD = KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.CTRL_MASK);
		saveToDatabase.setAccelerator(controlD);

		JMenuItem saveToXML = new JMenuItem(".XML");
		saveMenu.add(saveToXML);
		saveToXML.addActionListener(new MenuController(mfv).getSaveXmlAction());
		// Skr�t Ctrl+D zapisujacy DB
		KeyStroke controlS = KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK);
		saveToXML.setAccelerator(controlS);

		//	##	Podmenu LOAD
		JMenu loadMenu = new JMenu("Load from");
		fileMenu.add(loadMenu);
		JMenuItem loadFromDatabase = new JMenuItem("Database");
		loadMenu.add(loadFromDatabase);
		loadFromDatabase.addActionListener(new MenuController(mfv).getLoadSqlAction());
		KeyStroke controlL = KeyStroke.getKeyStroke(KeyEvent.VK_L, InputEvent.CTRL_MASK);
		loadFromDatabase.setAccelerator(controlL);


		JMenuItem loadFromXML = new JMenuItem(".XML");
		loadMenu.add(loadFromXML);
		loadFromXML.addActionListener(new MenuController(mfv).getLoadXmlAction());
		KeyStroke controlO = KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK);
		loadFromXML.setAccelerator(controlO);


		fileMenu.addSeparator();	//	SEPARATOR

		//	##	przycisk wyjscia z programu QUIT
		JMenuItem quit = new JMenuItem("QUIT");
		quit.addActionListener(new MenuController(mfv).getQuitAction());
		fileMenu.add(quit);
		KeyStroke controlQ = KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.CTRL_MASK);
		quit.setAccelerator(controlQ);


		//	#	Menu OPTIONS
		JMenu optionsMenu = new JMenu("Options");
		add(optionsMenu);

		//	##	Podmenu Peferrences
		JMenuItem preferences = new JMenuItem("Preferences");
		optionsMenu.add(preferences);
		preferences.addActionListener(new SettingsFrameController(null));
		KeyStroke controlP = KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.CTRL_MASK);
		preferences.setAccelerator(controlP);

		JMenuItem removeOlderThan = new JMenuItem("Remove older than...");
		optionsMenu.add(removeOlderThan);
		removeOlderThan.addActionListener(new MenuController(mfv).getRemoveOlderThanAction());
		KeyStroke controlR = KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_MASK);
		removeOlderThan.setAccelerator(controlR);

		//	#	Menu Info
		JMenu info = new JMenu("Info");
		add(info);

		//	##	Podmenu Peferrences
		JMenuItem about = new JMenuItem("About...");
		info.add(about);
		about.addActionListener(new MenuController(mfv).getAboutAction());
		KeyStroke f12 = KeyStroke.getKeyStroke(KeyEvent.VK_F12, InputEvent.CTRL_MASK);
		about.setAccelerator(f12);



	}


}










