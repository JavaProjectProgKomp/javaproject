package view;

import model.DayButtonModel;

import javax.swing.JButton;

import controller.DayButtonController;



/**
 * Klasa przycisku pokazujacego dzien
 */
public class DayButtonView extends JButton {
	/**
	 * Zmienna przechowuje widok g��wnego okna aplikacji
	 */
	private MainFrameView mainFrameView;
	/**
	 * Zmienna przechowuje model przycisku dnia.
	 */
	private DayButtonModel dayButtonModel;


	/* Konstruktor inicjalizuje podane zmienne oraz ustawia opis na podstawie numeru dnia miesiaca.
	 * Dodaje tak�e action listener do stworzonego przycisku dnia miesi�ca.
	 * @param dbm model przycisku dnia
	 * @param mfv widok g�wnego okna programu
	 */
	public DayButtonView(DayButtonModel dbm, MainFrameView mfv){
		this.mainFrameView = mfv;
		dayButtonModel=dbm;
		this.setText(new Integer(dayButtonModel.getNrDnia()).toString());
		this.addActionListener(new DayButtonController(this,this.mainFrameView));
	}

	/**
	 * Metoda zwraca model przycisku dnia.
	 * @return model przycisku dnia.
	 */
	public DayButtonModel getDayButtonModel(){
		return dayButtonModel;
	}

}








