package view;

import controller.EventsFrameController;
import model.EventsFrameModel;
import organizer.EventsList;
import editor.AddAlarmButtonEditor;
import renderer.AddAlarmButtonRenderer;
import editor.GetNoteButtonEditor;
import renderer.GetNoteButtonRenderer;
import editor.RemoveEventButtonEditor;
import renderer.RemoveEventButtonRenderer;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.RowFilter;



/**
 *	Klasa inicjalizuj�ca widok okna zawieraj�cego liste wydarze� z danego dnia w postaci tabeli.
 */
public class EventsFrameView extends JFrame {

	/**
	 * Zmienna widoku okna g��wnego aplikacji.
	 */
	private MainFrameView mainView;
	/**
	 * Zmienna modelu okna listy wydarze�.
	 */
	private EventsFrameModel model;
	/**
	 * Zmienna etykiety "WYDARZENIA".
	 */
	private JLabel l1 = new JLabel("WYDARZENIA");
	/**
	 * Zmienna prycisku "Dodaj wydarzenie".
	 */
	private JButton b1 = new JButton("Dodaj wydarzenie");

	/**
	 * Panel tytu�owy.
	 */
	private JPanel panelTitle = new JPanel();
	/**
	 * Panel listy wydarze�.
	 */
	private JPanel panelEventsList = new JPanel();
	/**
	 * Panel przycisk�w akcji.
	 */
	private JPanel panelActionsButtons = new JPanel();
	/**
	 * Tabela
	 */
	private JTable table;
	/**
	 * Panel przewijany dla tabeli.
	 */
	private JScrollPane scrollPane ;
	/**
	 * Pole s�u��ce fultrowaniu wydarze�.
	 */
	private JTextField filter = new JTextField();

	/**
	 * Konstruktor tworzy nowi widok tabeli wydarze�.
	 * @param m model okna tabeli wydarze�.
	 * @param mv widok okna g��wnego aplikacji.
	 */
	public EventsFrameView(EventsFrameModel m,MainFrameView mv){
		this.model=m;
		this.mainView=mv;

		//	definiowanie tabeli
		TableRowSorter<DefaultTableModel> sorter;
		sorter = new TableRowSorter<DefaultTableModel>(model.getTableModel());
		table = new JTable(model.getTableModel());
		table.setRowSorter(sorter);
		table.getColumn("Opis").setCellRenderer(new GetNoteButtonRenderer());
		table.getColumn("Opis").setCellEditor(new GetNoteButtonEditor(new JCheckBox(),this.table));
		table.getColumn("Ustaw").setCellRenderer(new AddAlarmButtonRenderer());
		table.getColumn("Ustaw").setCellEditor(new AddAlarmButtonEditor(new JCheckBox(),this.table,this));
		table.getColumn("Alarm").setMaxWidth(300);//setCellEditor(new AddAlarmButtonEditor(new JCheckBox(),this.table));
		table.getColumn("Usun wyd.").setCellRenderer(new RemoveEventButtonRenderer());
		table.getColumn("Usun wyd.").setCellEditor(new RemoveEventButtonEditor(new JCheckBox(),this.table,this));
		scrollPane = new JScrollPane(table);


		addCurrentEvents();

		//		ustawienie layoutu ramki
		setLayout(new BorderLayout());
		panelTitle.add(l1);
		panelTitle.setLayout(new FlowLayout());
		add(panelTitle,BorderLayout.NORTH);
		panelEventsList.setLayout(new FlowLayout());
		panelEventsList.add(scrollPane);
		panelEventsList.add(filter);
		filter.setPreferredSize(new Dimension(400,40));
		filter.setBorder(BorderFactory.createTitledBorder("Filtruj: "));
		filter.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void insertUpdate(DocumentEvent e) {
				filterTextKeyRelased(e);
			}
			@Override
			public void removeUpdate(DocumentEvent e)
			{
				filterTextKeyRelased(e);
			}
			@Override
			public void changedUpdate(DocumentEvent e) {
				filterTextKeyRelased(e);
			}
		});
		add(panelEventsList,BorderLayout.CENTER);
		panelActionsButtons.add(b1);

		//	klikniecie przycisku dodajace wydarzenie do listy wydarzen z odpowiednia data
		b1.addActionListener (new EventsFrameController(this));

		add(panelActionsButtons,BorderLayout.SOUTH);

		//	ustawienie rozmiaru okna i jego pozycji na srodek okna
		setSize(600,580);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		setVisible(true);
		setTitle(this.model.getEvent().getDate().toString());
		setResizable(true);

	}

	/**
	 * Metoda slu��ca do filtrowania wydarze�
	 * @param query zapytanie
	 */
	private void newFilter(String query) {
		TableRowSorter <DefaultTableModel> trs = new TableRowSorter<DefaultTableModel>(model.getTableModel());
		table.setRowSorter(trs);
		trs.setRowFilter(RowFilter.regexFilter(query));
	}
	/**
	 * Metoda obs�uguje zapytanie filtra po ka�dym dodanym znaku.
	 * @param e
	 */
	private void filterTextKeyRelased(DocumentEvent e){
		String query = filter.getText();
		newFilter(query);
	}

	/**
	 * Dodaje wydarenie danego dnia to listy wszystkich wydarze�.
	 */
	public void addEventToList(){
		EventsList.getArray().add(model.getEvent());

	}
	/**
	 * Dodaje wydarzenia z danego dnia do modelu tabeli, kt�ra ma zosta� wy�wietlona.
	 */
	public void addCurrentEvents(){

		Object[] retVal = new Object[6];

		for(int i =0; i!=EventsList.getArray().size(); i++){

			if(EventsList.getArray().get(i).getDate().getDayOfMonth() == model.getDate().getDayOfMonth() &&
					EventsList.getArray().get(i).getDate().getMonthValue() == model.getDate().getMonthValue() &&
					EventsList.getArray().get(i).getDate().getYear() == model.getDate().getYear() ){

				String time = EventsList.getArray().get(i).getTime().toString();
				String title = EventsList.getArray().get(i).getTitle();
				String note = EventsList.getArray().get(i).getNote();

				retVal [0] = time;
				retVal [1] = title;
				retVal [2] = note;
				retVal [3] = "+";
				retVal [4] = EventsList.getArray().get(i).getAlarmDate()+" "+EventsList.getArray().get(i).getAlarmTime();
				retVal [5] = "-";

				model.addRowToTable(retVal);
				//this.refreshView();

				System.out.println("####");

			}
		}
	}
	/**
	 * Od�wie�a widok okna.
	 */
	public void refreshView(){
		this.validate();
		this.repaint();
	}
	/**
	 * Zamyka widok okna.
	 */
	public void closeView(){
		this.dispose();
	}
	//	GETTERY
	/**
	 * Zwraca model okna wydarzen.
	 * @return model okna wydarzen.
	 */
	public EventsFrameModel getModel(){
		return this.model;
	}
	/**
	 * Zwraca widok okna g��wnego.
	 * @return widok okna g��wnego.
	 */
	public MainFrameView getMainFrameView(){
		return this.mainView;
	}
}




