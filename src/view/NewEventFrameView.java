package view;

import model.NewEventFrameModel;
import organizer.Event;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import controller.NewEventFrameController;



/**
 * Klasa widoku dla klasy NewEventFrame
 */
public class NewEventFrameView extends JFrame{

	/**
	 * Widok g��wnego okna programu.
	 */
	private MainFrameView mainFrameView;	//	widok glownego okna programu
	/**
	 * Model okna dodaj�cego nowe wydarzenie.
	 */
	private NewEventFrameModel model = new NewEventFrameModel();
	/**
	 * Zmienna przechowuj�ca wydarzenie typu Event.
	 */
	private Event event;
	/**
	 * Zmienna typu JComboBox wyboru godziny.
	 */
	private JComboBox hour;
	/**
	 * Zmienna typu JComboBox wyboru minuty wydarzenia.
	 */
	private JComboBox minute;
	/**
	 * G�rny panel okna g��wnego wydarzenia.
	 */
	private JPanel panelUp = new JPanel();
	/**
	 * �rodkowy panel okna g��wnego wydarzenia.
	 */
	private JPanel panelCenter = new JPanel();
	/**
	 * Dolny panel okna g��wnego wydarzenia.
	 */
	private JPanel panelDown = new JPanel();
	/**
	 * Pole tekstowe zawieraj�ce tytu� okna.
	 */
	private JTextField title;
	/**
	 * Pole tekstowe zawiej�ce opis.
	 */
	private JTextArea note;
	/**
	 * Przycisk potwierdzaj�cy dodanie wydarzenia.
	 */
	private JButton ok;




	/**
	 * Konstruktor tworz�cy okno obs�uguj�ce dodawanie nowego wydarzenia.
	 * @param e wydarzenie .
	 * @param mfv widok okna g��wnego aplikacji.
	 */
	public NewEventFrameView(Event e, MainFrameView mfv){
		this.mainFrameView=mfv;
		this.event=e;

		//	Ustawia tytul okna z data danego dnia
		setTitle("Dodaj wydarzenie do dnia: "+this.getEvent().getDate());

		//		ustawienie rozmiaru okna i jego pozycji na srodek okna
		setSize(500,400);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		setVisible(true);
		setLayout(new BorderLayout());
		add(panelUp,BorderLayout.NORTH);
		add(panelCenter,BorderLayout.CENTER);

		panelUp.setBorder(BorderFactory.createTitledBorder("Wybierz godzine: "));
		hour = new JComboBox(model.getHoursString());
		panelUp.add(hour);

		JLabel l2 = new JLabel(":");
		panelUp.add(l2);


		minute = new JComboBox(model.getMinutesString());
		panelUp.add(minute);
		panelUp.setLayout(new FlowLayout());

		panelCenter.setLayout(new BorderLayout());
		panelCenter.setBorder(BorderFactory.createTitledBorder("Tytul oraz opis"));
		title = new JTextField();
		title.setToolTipText("Please enter some text here");
		title.setPreferredSize(new Dimension(150,20));
		panelCenter.add(title,BorderLayout.NORTH);

		String str = title.getText();
		System.out.println(str);

		note = new JTextArea(8,40);
		JScrollPane scroll = new JScrollPane(note);
		panelCenter.add(scroll,BorderLayout.CENTER);


		ok = new JButton("OK");
		ok.addActionListener(new NewEventFrameController(this,this.mainFrameView));	//	przekazanie widoku ramki wydarzen danego dna oraz widoku ramku okna glownego
		panelDown.add(ok);
		add(panelDown,BorderLayout.SOUTH);

		setLayout(new FlowLayout());

	}

	/**
	 * Zwraca wydarzenie.
	 * @return wydarzenie.
	 */
	public Event getEvent(){
		return this.event;
	}
	/**
	 * Zwraca godzine.
	 * @return godzine.
	 */
	public JComboBox getHour(){
		return this.hour;
	}
	/**
	 * Zwraca minuty.
	 * @return minuty.
	 */
	public JComboBox getMinutes(){
		return this.minute;
	}
	/**
	 * Zwraca pole tekstowe z tytu�em.
	 * @return pole tekstowe z tytu�em.
	 */
	public JTextField getTitleField(){
		return this.title;
	}
	/**
	 * Zwraca pole tekstowe z opisem.
	 * @return pole tekstowe z opisem.
	 */
	public JTextArea getNoteArea(){
		return this.note;
	}

	/**
	 * Od�wie�a widok.
	 */
	public void refreshView(){
		this.validate();
		this.repaint();
	}

}

