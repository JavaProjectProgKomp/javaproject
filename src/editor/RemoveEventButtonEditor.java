package editor;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;

import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;

import organizer.EventsList;
import model.EventsFrameModel;
import view.EventsFrameView;

/**
 * Klasa rozszerzająca DefaultCellEditor dla przycisku usuwania wydarzeń.
 */
public class RemoveEventButtonEditor extends DefaultCellEditor {

	protected JButton button;
	private String label;
	private boolean isPushed;
	private EventsFrameView efv;		//	Widok okna zawierajacego tabele ustawionych wydarzen
	/**
	 * Konstruktor
	 * @param checkBox
	 * @param table
	 * @param efv
	 */
	public RemoveEventButtonEditor(JCheckBox checkBox, JTable table, EventsFrameView efv) {

		super(checkBox);
		this.efv=efv;

		button = new JButton();
		button.addActionListener(new ActionListener(){

			//	ActionListener usuwajacy wydarzenie z listy
			@Override
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.showConfirmDialog(button, "Do you want to delete this event?");

				int row = table.getSelectedRow();					//	pobranie wartosci wiersza
				String title = (String)table.getValueAt(row, 1);	//	pobranie tytulu wydarzenia z podanej komorki tabeli
				//				System.out.println(title);

				if(dialogButton == JOptionPane.YES_OPTION){

					// metoda getEventIndex zwraca index eventu o podanym tytule
					EventsList.getArray().remove(EventsList.getEventIndex(title));
					efv.getMainFrameView().rysujMiesiac();	//	odswiezenie widoku glownego okna

					//	Problem z odswiezeniem okna widoku listy wydarzen z danego dnia
					// rozwiazano za pomoca zamkniecia okna i ponownego stworzenia go ***!
					efv.closeView();
					new EventsFrameView(new EventsFrameModel(LocalDate.now()),efv.getMainFrameView());
				}

			}

		});
		button.setOpaque(true);
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				fireEditingStopped();
			}
		});
	}

	@Override
	/**
	 *
	 */
	public Component getTableCellEditorComponent(JTable table, Object value,
			boolean isSelected, int row, int column) {
		if (isSelected) {
			button.setForeground(table.getSelectionForeground());
			button.setBackground(table.getSelectionBackground());
		} else {
			button.setForeground(table.getForeground());
			button.setBackground(table.getBackground());
		}
		label = (value == null) ? "" : value.toString();
		button.setText(label);
		isPushed = true;
		return button;
	}

	@Override
	public Object getCellEditorValue() {
		if (isPushed) {

		}
		isPushed = false;
		return new String(label);
	}

	@Override
	public boolean stopCellEditing() {
		isPushed = false;
		return super.stopCellEditing();
	}

	@Override
	protected void fireEditingStopped() {
		super.fireEditingStopped();
	}
}