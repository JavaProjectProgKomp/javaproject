package editor;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;

import organizer.EventsList;
import view.EventsFrameView;

/**
 * Klasa interfejsu edycji kom�rki tabeli wydarzen
 */
public class AddAlarmButtonEditor extends DefaultCellEditor {

	/**
	 * Przycisk
	 */
	protected JButton button;
	/**
	 *
	 */
	private String label;
	/**
	 * Zmienna okreslajaca czy przycisk jest wci�ni�ty
	 */
	private boolean isPushed;
	private EventsFrameView efv;

	public AddAlarmButtonEditor(JCheckBox checkBox, JTable table,EventsFrameView efv) {

		super(checkBox);
		this.efv=efv;

		button = new JButton();
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {

				int row = table.getSelectedRow();
				String title = (String)table.getValueAt(row, 1);
				int index = EventsList.getEventIndex(title);

				if(EventsList.getArray().get(index)!=null){
					int overwrite = JOptionPane.showConfirmDialog(checkBox, "Do you want change alarm date?");
					if(overwrite == JOptionPane.YES_OPTION){

						String dateStr = JOptionPane.showInputDialog(null, "yyyy-MM-dd", "Set alarm date", JOptionPane.PLAIN_MESSAGE );
						DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
						LocalDate alarmLocalDate = LocalDate.parse(dateStr, dtf);
						EventsList.getArray().get(index).setAlarmDate(alarmLocalDate);

						String timeStr = JOptionPane.showInputDialog(null, "hh:mm", "Set alarm time", JOptionPane.PLAIN_MESSAGE )+":00";
						LocalTime alarmLocalTime = LocalTime.parse(timeStr);
						EventsList.getArray().get(EventsList.getEventIndex(title)).setAlarmTime(alarmLocalTime);

						refreshEventsFrameView(efv);
						efv.dispose();
					}
				}


				System.out.println(EventsList.getArray().get(EventsList.getEventIndex(title)).getAlarmDate());
				System.out.println(EventsList.getArray().get(EventsList.getEventIndex(title)).getDate());


			}

		});
		button.setOpaque(true);
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				fireEditingStopped();
			}
		});
	}

	public void refreshEventsFrameView(EventsFrameView efv){
		efv.revalidate();
		efv.repaint();
	}

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value,
			boolean isSelected, int row, int column) {
		if (isSelected) {
			button.setForeground(table.getSelectionForeground());
			button.setBackground(table.getSelectionBackground());
		} else {
			button.setForeground(table.getForeground());
			button.setBackground(table.getBackground());
		}
		label = (value == null) ? "" : value.toString();
		button.setText(label);
		isPushed = true;
		return button;
	}

	/* (non-Javadoc)
	 * @see javax.swing.DefaultCellEditor#getCellEditorValue()
	 */
	@SuppressWarnings("null")
	@Override
	public Object getCellEditorValue() {
		if (isPushed) {

		}
		isPushed = false;
		return new String(label);
	}

	@Override
	public boolean stopCellEditing() {
		isPushed = false;
		return super.stopCellEditing();
	}

	@Override
	protected void fireEditingStopped() {
		super.fireEditingStopped();
	}
}

