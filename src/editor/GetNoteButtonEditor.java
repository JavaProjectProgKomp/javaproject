package editor;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;

/**
 * Klasa edytora kom�rki przycisku pokazuj�cego opis wydarzenia.
 */
public class GetNoteButtonEditor extends DefaultCellEditor {

	/**
	 * Zmienna przycisku.
	 */
	protected JButton button;
	/**
	 * Zmienna opisu typu String
	 */
	private String label;
	/**
	 * Zmienna logiczna wskazuj�ca na wci�ni�cie przycisku.
	 */
	private boolean isPushed;

	public GetNoteButtonEditor(JCheckBox checkBox, JTable table) {
		super(checkBox);

		button = new JButton();
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {


				int row = table.getSelectedRow();					//	pobranie wartosci wiersza
				String note = (String)table.getValueAt(row, 2);	//	pobranie tytulu wydarzenia z podanej komorki tabeli
				//				System.out.println(title);

				String title = "Note: "+(String)table.getValueAt(row, 0)+" "+(String)table.getValueAt(row, 1);

				Object[] options = {"OK"};
				JOptionPane.showOptionDialog(new JFrame(),
						note,title,
						JOptionPane.PLAIN_MESSAGE,
						JOptionPane.QUESTION_MESSAGE,
						null,
						options,
						options[0]);


			}

		});
		button.setOpaque(true);
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				fireEditingStopped();
			}
		});
	}

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value,
			boolean isSelected, int row, int column) {
		if (isSelected) {
			button.setForeground(table.getSelectionForeground());
			button.setBackground(table.getSelectionBackground());
		} else {
			button.setForeground(table.getForeground());
			button.setBackground(table.getBackground());
		}
		label = (value == null) ? "" : value.toString();
		button.setText(label);
		isPushed = true;
		return button;
	}

	/* (non-Javadoc)
	 * @see javax.swing.DefaultCellEditor#getCellEditorValue()
	 */
	@SuppressWarnings("null")
	@Override
	public Object getCellEditorValue() {
		if (isPushed) {

		}
		isPushed = false;
		return new String(label);
	}

	@Override
	public boolean stopCellEditing() {
		isPushed = false;
		return super.stopCellEditing();
	}

	@Override
	protected void fireEditingStopped() {
		super.fireEditingStopped();
	}
}
