package model;

/**
 * Klasa modelu okna ustawie�.
 */
public class SettingsFrameModel {
	String theme;

	/**
	 * Konstruktor domy�lny.
	 */
	public SettingsFrameModel(){

	}

	/**
	 * Konstruktor inicjalizuj�cy zmienn� z nazw� motywu graficznego.
	 * @param theme nazwa motywu graficznego.
	 */
	public void setTheme(String theme){
		this.theme=theme;
	}
	/**
	 * Metoda zwraca nazw� motywu graficznego.
	 * @return nazw� motywu graficznego.
	 */
	public String getTheme(){
		return this.theme;
	}

}
