package model;

import java.time.LocalDate;
import java.util.ArrayList;

import view.DayButtonView;

/**
 * Klasa przechowuje wszystkie dane,
 * kt�re wykorzystuje g��wne okno aplikacji.
 *
 */
public class MainFrameModel  {
	/**
	 * Reprezentuje miesiac w postaci liczby.
	 */
	private int nrMiesiaca;
	/**
	 * Reprezentuje rok w postaci liczby.
	 */
	private int nrRoku;
	/**
	 * Lista, kt�ra przechowuje przyciski dni, danego miesiaca w roku
	 */
	private ArrayList<DayButtonView> buttonList;
	/**
	 * Tablica String�w z nazwami mieci�cy.
	 */
	final private String[] miesiace =
		{"- miesiac -","Styczen","Luty","Marzec","Kwiecien","Maj","Czerwiec","Lipiec",
				"Sierpien","Wrzesien","Pazdziernik","Listopad","Grudzien"
		};
	/**
	 * Tablica String�w ze skr�tami nazw dni tygodnia.
	 */
	final private String[] dni =
		{"PN","WT","SR","CZW","PT","SOB","ND"};

	/**
	 * Konstruktor domy�lny, kt�ry inicjalizuje zmienne buttonList, nrMiesiaca, nrRoku.
	 */
	public MainFrameModel(){

		buttonList = new ArrayList<DayButtonView>();
		nrMiesiaca = LocalDate.now().getMonthValue();
		nrRoku = LocalDate.now().getYear();

	}

	/**
	 * Konstruktor domy�lny, kt�ry inicjalizuje zmienne buttonList, nrMiesiaca, nrRoku przekazan� dat�.
	 * @param ld inicjalizuje poszczegolne dane czasu.
	 */
	public MainFrameModel(LocalDate ld){

		buttonList = new ArrayList<DayButtonView>();
		nrMiesiaca = ld.getMonthValue();
		nrRoku = ld.getYear();
	}

	//	GETTERY
	/**
	 * Zwraca int nr miesiaca.
	 * @return nr miesiaca.
	 */
	public int getNrMiesiaca(){
		return this.nrMiesiaca;
	}

	/**
	 * Zwraca rok w postaci zmiennej typu int.
	 * @return rok
	 */
	public int getnrRoku(){
		return this.nrRoku;
	}
	/**
	 * Metoda zwraca liste przycisk�w obs�uguj�cych dany dzie� miesi�ca.
	 * @return liste przycisk�w obs�uguj�cych dany dzie� miesi�ca.
	 */
	public ArrayList<DayButtonView> getButtonList(){
		return this.buttonList;
	}
	/**
	 * Metoda zwraca tablice String�w z nazwami miesi�cy.
	 * @return tablice String�w z nazwami miesi�cy.
	 */
	public String[] getStringMiesiace(){
		return this.miesiace;
	}
	/**
	 * Metoda zwraca tablice String�w z numerami dni danego miesiaca.
	 * @return tablice String�w z numerami dni danego miesiaca.
	 */
	public String[] getStringDni(){
		return this.dni;
	}


	/**
	 * Ustawia numer miesiaca.
	 * @param m numer miesiaca.
	 */
	public void setNrMiesiaca(int m){
		this.nrMiesiaca=m;
	}
	/**
	 * Ustawia rok.
	 * @param r rok.
	 */
	public void setNrRoku(int r){
		this.nrRoku=r;
	}
	/**
	 * Ustawia liste przycisk�w dni.
	 * @param bl array list przyciskow dni.
	 */
	public void setButtonList(ArrayList<DayButtonView> bl){
		this.buttonList=bl;
	}
	/**
	 * �aduje ustawienia aplikacji z pliku config.cfg.
	 */
	public void loadConfig(){

	}


}