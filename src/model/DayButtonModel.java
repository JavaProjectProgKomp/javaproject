package model;

/**
 * Klasa modelu przycisku dnia.
 */
public class DayButtonModel {
	/**
	 * Numer dnia
	 */
	private int nrDnia;
	/**
	 * Numer miesi�ca
	 */
	private int nrMiesiaca;
	/**
	 * Numer roku
	 */
	private int nrRok;

	/**
	 * Konstruktor domy�lny.
	 */
	public DayButtonModel(){

	}

	/**
	 * Konstruktor inicjalizuje wszystkie zmienne czasu.
	 * @param nrDnia numer dnia miesiaca.
	 * @param nrMiesiaca numer miesiaca (1-12).
	 * @param nrRoku numer roku.
	 */
	public DayButtonModel(int nrDnia,int nrMiesiaca, int nrRoku){
		this.nrDnia=nrDnia;
		this.nrMiesiaca=nrMiesiaca;
		this.nrRok=nrRoku;
	}

	/**
	 * Metoda ustawia numer dnia w miesi�cu.
	 * @param nrDnia numer dnia
	 */
	public void setNrDnia(int nrDnia){
		this.nrDnia=nrDnia;
	}

	/**
	 * Metoda ustawia numer miesiaca w roku (1-12).
	 * @param nrMiesiaca numer miesiaca.
	 */
	public void setNrMiesiaca(int nrMiesiaca){
		this.nrMiesiaca=nrMiesiaca;
	}

	/**
	 * Metoda ustawia rok
	 * @param nrRoku roku.
	 */
	public void setNrRoku(int nrRoku){
		this.nrRok=nrRoku;
	}


	/**
	 * Zwraca numer dnia.
	 * @return - numer dnia.
	 */
	public int getNrDnia(){
		return this.nrDnia;
	}
	/**
	 * Zwraca numer miesiaca.
	 * @return - numer miesi�ca
	 */
	public int getNrMiesiaca(){
		return this.nrMiesiaca;
	}
	/**
	 * Zwraca rok w postaci inta.
	 * @return - rok w postaci inta.
	 */
	public int getNrRoku(){
		return this.nrRok;
	}
}
