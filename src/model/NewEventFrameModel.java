package model;

/**
 * Klasa modelu ramki wydarzen danego dnia
 */
public class NewEventFrameModel {

	/**
	 * Zmienna przechowująca godziny całej doby.
	 */
	final private String[] hours = {"00","01","02","03","04","05","06","07","08","09","10",
			"11","12","13","14","15","16","17","18","19","20","21","22","23"};
	/**
	 * Tablica przechowująca minuty od 00 do 59
	 */
	private String[] minutes;

	/**
	 * Konstruktor domyślny który wypełnia tablice przechowującą minuty wartościami od 00 do 59.
	 */
	public NewEventFrameModel(){
		// Wypelnia tablice Stringow minutami od 0 do 59
		this.minutes = new String[60];
		for(int i=0;i!=60;i++){
			if(i<10){
				minutes[i]="0"+new Integer(i).toString();
			}
			else
				minutes[i]=new Integer(i).toString();
		}
	}

	/**
	 * Metoda zwraca tablice stringów przechowującą wartości godzin w dobie.
	 * @return tablice stringów przechowującą wartości godzin w dobie.
	 */
	public String[] getHoursString(){
		return this.hours;
	}
	/**
	 * Metoda zwraca tablice stringów przechowującą wartości minut (00-59).
	 * @return tablice stringów przechowującą wartości minut (00-59).
	 */
	public String[] getMinutesString(){
		return this.minutes;
	}
}
