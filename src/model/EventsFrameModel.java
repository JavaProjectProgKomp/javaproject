package model;

import java.time.LocalDate;

import javax.swing.table.DefaultTableModel;

import organizer.Event;

/**
 * Klasa (model) okna z tabel� wydarze� danego dnia.
 *
 */
public class EventsFrameModel {

	/**
	 * Data wydarzenia.
	 */
	private LocalDate date;
	/**
	 * Wydarzenie (obiekt klasy Event).
	 */
	private Event event;
	/**
	 * Nazwy kolumn w tabeli wydarze�.
	 */
	private String[] columnNames = {"Godzina","Tytul","Opis","Ustaw","Alarm","Usun wyd."};
	/**
	 * Model tabeli.
	 */
	private DefaultTableModel tableModel = new DefaultTableModel(columnNames, 0);	// The 0 argument is number rows.

	/**
	 * Konstruktor inicjalizuj�cy zmienna date oraz tworz�cy na jej podstawie nowe wydarzenie (new Event(ld)).
	 * @param ld
	 */
	public EventsFrameModel(LocalDate ld){


		this.date = ld;
		event = new Event(ld);
	}

	/**
	 * ustawia wydarzenie (Event).
	 * @param e wydarzenie (Event).
	 */
	public void setEvent(Event e){
		this.event=e;
	}

	/**
	 * Zwraca wydarzenie (Event).
	 * @return wydarzenie (Event).
	 */
	public Event getEvent(){
		return this.event;
	}
	/**
	 * Zwraca numer dnia z miesiaca ustawionego w zmiennej date.
	 * @return numer dnia.
	 */
	public int getDay(){
		return this.date.getDayOfMonth();
	}
	/**
	 * Zwraca numer miesiaca z roku ustawionego w zmiennej date.
	 * @return numer miesi�ca w postaci inta.
	 */
	public int getMonth(){
		return this.date.getMonthValue();
	}
	/**
	 * Zwraca rok zawarty w zmiennej date.
	 * @return rok w
	 */
	public int getYear(){
		return this.date.getYear();
	}
	/**
	 * Zwraca ca�� zmienn� date.
	 * @return date.
	 */
	public LocalDate getDate(){
		return this.date;
	}
	/**
	 * Zwraca model tabeli.
	 * @return model tabeli.
	 */
	public DefaultTableModel getTableModel(){
		return this.tableModel;
	}

	/**
	 * Dodaje obiekt do modelu tabeli.
	 * @param o obiekt do dodania.
	 */
	public void addRowToTable(Object[] o){
		tableModel.addRow(o);
	}


}
