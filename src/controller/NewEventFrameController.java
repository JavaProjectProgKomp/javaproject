package controller;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalTime;

import model.EventsFrameModel;
import organizer.EventsList;
import view.EventsFrameView;
import view.MainFrameView;
import view.NewEventFrameView;

/**
 * Klasa kontrolera okna obsługującego dodawanie nowych wydarzeń.
 */
public  class NewEventFrameController implements ActionListener{

	/**
	 * Widok okna obsługującego dodawanie nowego wydarzenia do listy wydarzeń.
	 */
	private NewEventFrameView newEventFrameView;
	/**
	 * Widok okna głównego aplikacji.
	 */
	private MainFrameView mainFrameView;

	/**
	 * Konstruktor inicjalizujący zmienne widoku okna dodającego nowe wydarzenie oraz widoku okna głównego aplikacji.
	 * @param nefv widok nowego okna dodającego wydarzenie.
	 * @param mfv widok okna głównego aplikacji.
	 */
	public  NewEventFrameController(NewEventFrameView nefv, MainFrameView mfv){
		this.newEventFrameView = nefv;
		this.mainFrameView = mfv;
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public void actionPerformed(ActionEvent arg0) {


		if(!newEventFrameView.getTitleField().getText().isEmpty()){

			// Za pomoca metod z klasy newEventFrameView pobierany jest tytul oraz notatka
			// rzeczy te sa od razu przypisywane do odpowiednich pol klasy Event
			newEventFrameView.getEvent().setTitle( newEventFrameView.getTitleField().getText() );
			newEventFrameView.getEvent().setNote( newEventFrameView.getNoteArea().getText() );
			//	zmienne z przypisana godzina ktora uzytkownik wybral z comboBoxow
			int h = Integer.parseInt( newEventFrameView.getHour().getSelectedItem().toString() );
			int m = Integer.parseInt( newEventFrameView.getMinutes().getSelectedItem().toString() );
			//	ustawienie czasu lokalnego
			LocalTime lt = LocalTime.of(h, m);
			//	ustawienie danego czasu w wydarzeniu
			newEventFrameView.getEvent().setTime( lt );

			EventsList.getArray().add(newEventFrameView.getEvent());

			//			// ##########################################################
			//			System.out.println(lt);
			//			System.out.println(newEventFrameView.getEvent().getDate());
			//			System.out.println(newEventFrameView.getEvent().getTitle());
			//			System.out.println(newEventFrameView.getEvent().getNote());
			//			//	#########################################################

			// Zamyka okno bez zamykania calego programu

			this.newEventFrameView.dispose();	// zamyka okno
			this.mainFrameView.rysujMiesiac();	//	wywoluje metode rysujaca miesiaca, ktora uruchamia metode odsiezenia widoku
			EventsFrameView efv = new EventsFrameView(new EventsFrameModel(newEventFrameView.getEvent().getDate()),this.mainFrameView);

		}
		else{

			//	Nie pozwala dodac wydarzenia bez tytulu i ustawia tlo na czerwone
			newEventFrameView.getTitleField().setBackground(Color.RED);

		}

	}

}

