package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import organizer.Event;
import organizer.EventsList;
import view.MainFrameView;

/**
 * Klasa kontrolera Menu
 */
public class MenuController implements ActionListener  {
	/**
	 * Zmienna widoku glownego okna aplikacji (umozliwia odswiezenie widoku po wczytaniu pliku
	 */
	private MainFrameView mfv;		//

	/**
	 *Klasa wewnetrza obslugujaca zapis danych do pliku XML z pomoca okna wyboru pliku (JFileChooser)
	 */
	class SaveXmlAction implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {

			// Zapis wydarzen do zmiennej w formacie XML
			XStream xstream = new XStream(new DomDriver());
			String xml = xstream.toXML(EventsList.getArray());

			//	Kod wyswietlajacy okno wyboru pliku
			JFileChooser chooser = new JFileChooser();
			chooser.setCurrentDirectory(new File("."));
			int retrival = chooser.showSaveDialog(null);

			// Warunek sprawdzajacy czy nazwa pod jaka chcemy zapisac plik juz istenieje
			if(chooser.getSelectedFile().exists() && chooser.getDialogType() == JFileChooser.SAVE_DIALOG){
				int result = JOptionPane.showConfirmDialog(chooser,"The file exists, overwrite?","Existing file",JOptionPane.YES_NO_CANCEL_OPTION);
				switch(result){
				case JOptionPane.YES_OPTION:
					chooser.approveSelection();
					return;
				case JOptionPane.NO_OPTION:
					return;
				case JOptionPane.CLOSED_OPTION:
					return;
				case JOptionPane.CANCEL_OPTION:
					chooser.cancelSelection();
					return;
				}
			}
			else if (retrival == JFileChooser.APPROVE_OPTION) {

				String pathStr = chooser.getSelectedFile().getPath(); 	// Zmienna przechowuje sciezke do pliku
				//	Warunek sprawdzajacy czy wpisalismy tez rozszerzenie pliku jako xml czy nie
				if(pathStr.substring(pathStr.length()-3).equals("xml")){
					FileWriter fw;
					try {
						fw = new FileWriter(pathStr);
						fw.write(xml);
						fw.close();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

				}
				//	jezeli nie dodalismy rozszerzenia xml to dodane zostanie ono automatycznie
				else{
					FileWriter fw;
					try {
						fw = new FileWriter(pathStr+".xml");
						fw.write(xml);
						fw.close();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

				}

			}
		}
	}
	/**
	 *Klasa wewn�trzna obs�uguj�ca odczyt danych z pliku XML.
	 */
	class LoadXmlAction implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			// Zapis wydarzen do zmiennej w formacie XML
			XStream xstream = new XStream(new DomDriver());


			//			Kod wyswietlajacy okno wyboru pliku
			JFileChooser chooser = new JFileChooser();
			FileNameExtensionFilter filter = new FileNameExtensionFilter("XML FILES", "xml", "XML");	// Filtr ktory po ustawieniu pokazuje tylko pliku .xml
			chooser.setFileFilter(filter);
			chooser.setCurrentDirectory(new File("."));		//	ustawienie widoku w oknie na katalog programu
			int retrival = chooser.showOpenDialog(null);
			if (retrival == JFileChooser.APPROVE_OPTION) {
				try {
					//	Wczytywanie pliku do zmiennej String a nastepnie rzutowanie go za pomoca xstream do listy wydarzen
					File file = chooser.getSelectedFile();
					FileInputStream fis = new FileInputStream(file);
					byte[] data = new byte[(int) file.length()];
					fis.read(data);
					fis.close();
					String str = new String(data, "UTF-8");
					System.out.println(str);
					EventsList.setArray((ArrayList) xstream.fromXML(str));
					refresh();

				}
				catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
	}

	/**
	 * Klasa wewn�trzna obs�uguj�ca zapisa danych do bazy danych.
	 */
	class SaveSqlAction implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			String url = "jdbc:sqlserver://localhost;databaseName=EVENTS;integratedSecurity=true;";
			//String username = "JavaOrganizer";
			//String password = "password";
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				Connection conn = DriverManager.getConnection(url);
				Statement stmt = conn.createStatement();



				// Sprawdza czy Singleton zawiera jakies wydarzenia
				if(EventsList.getArray().size()!=0){
					stmt.execute("DELETE from Events");
					//	Petla wszystkich wydarzen
					for (int i=0; i!=EventsList.getArray().size();i++) {
						//	Sprawdza czy wydarzenie posiada alarm
						if(EventsList.getArray().get(i).getAlarmDate()!=null &&
								EventsList.getArray().get(i).getAlarmTime()!=null){
							toSqlWithAlarm(i,stmt);
						}
						else{
							toSqlWithoutAlarm(i,stmt);
						}
					}

					okFrame("Action completed successfully!","Save to database");
				}
				else{
					okFrame("Nothing to save.","Save to database");
				}

				conn.close();

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}


	}
	/**
	 * Klasa wewn�trzna obs�uguj�ca odczyt danych z bazy danych.
	 */
	class LoadSqlAction implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			String url = "jdbc:sqlserver://localhost;databaseName=EVENTS;integratedSecurity=true;";
			//String username = "JavaOrganizer";
			//String password = "password";
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				Connection conn = DriverManager.getConnection(url);
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery("Select dzien,godzina,tytul,opis,dzienAlarm,czasAlarm from Events");

				// Sprawdza czy w bazie danych znajduja sie jakies rekordy
				if(!rs.wasNull()){
					EventsList.getArray().clear();
					int i = 0;

					while(rs.next()){

						LocalDate ld = LocalDate.parse(rs.getString("dzien"));
						LocalTime lt = LocalTime.parse(rs.getString("godzina"));
						String title = rs.getString("tytul");
						String note = rs.getString("opis");

						//						System.out.println(ld.toString());
						//						System.out.println(lt.toString());
						//						System.out.println(title);
						//						System.out.println(note);

						EventsList.getArray().add(new Event(ld,lt,title,note));

						if(rs.getString("dzienAlarm")!=null){
							LocalDate lda = LocalDate.parse(rs.getString("dzienAlarm"));
							EventsList.getArray().get(i).setAlarmDate(lda);

							//							System.out.println(lda.toString());
						}
						if(rs.getString("czasAlarm")!=null){
							LocalTime lta = LocalTime.parse(rs.getString("czasAlarm"));
							EventsList.getArray().get(i).setAlarmTime(lta);

							//							System.out.println(lta.toString());
						}
						i++;
					}

					refresh();

					okFrame("Action completed successfully!","Load from database");
				}
				else{
					okFrame("Nothing to load.","Load from database");
				}

				conn.close();

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * Klasa wew. posiadajaca kontoler wyswietlajacy okienko "o programie".
	 */
	class About implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			JOptionPane.showMessageDialog(null, "Authors: \n Przemys�aw Rzepka \n Rados�aw K�pa \n\n This application was created within \n the university project. \n This is a simple calendar / organizer \n with graphical interface, written in java."
					,"About...",JOptionPane.PLAIN_MESSAGE,null);

		}

	}

	/**
	 * Klasa wew. posiadajaca kontroler usuwajacy stare wydarzenia.
	 */
	class RemoveOlderThan implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			String data = JOptionPane.showInputDialog("YYYY-mm-DD");
			LocalDate ld = LocalDate.parse(data);

			if(!EventsList.getArray().isEmpty()){
				for(int i =0; i!=EventsList.getArray().size();i++){

					if(EventsList.getArray().get(i).getDate().isBefore(ld)){
						EventsList.getArray().remove(i);
					}

				}
			}

			refresh();
		}

	}

	/**
	 * klasa wewn�trzna zawierajaca akcje wyjscia z programu.
	 */
	class QuitAction implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			int i = JOptionPane.showConfirmDialog(new JFrame(), "Do you really want to quit?");
			if(i==JOptionPane.OK_OPTION){
				System.exit(0);
			}

		}
	}


	/**
	 * Konstruktor domy�lny kontrolera menu.
	 */
	public MenuController(){

	}
	/**
	 * Konstruktor inicjalizuj�cy zmienn� mfv zmienn� podan� w argumencie.
	 * @param mfv - zmienna widoku okna g��wnego aplikacji.
	 */
	public MenuController(MainFrameView mfv){
		this.mfv=mfv;

	}
	/**
	 * Metoda tworzy i zwraca klase obs�uguj�c� zapis danych do pliku XML.
	 * @return Klase akcji zapisu do pliku.
	 */
	public SaveXmlAction getSaveXmlAction(){
		return new SaveXmlAction();
	}
	/**
	 * Metoda tworzy i zwraca klase obs�uguj�c� odczyt danych z pliku XML.
	 * @return Klase akcji odczytu z pliku.
	 */
	public LoadXmlAction getLoadXmlAction(){
		return new LoadXmlAction();
	}
	/**
	 *  Metoda tworzy i zwraca klase obs�uguj�c� zapis danych do bazy danych.
	 * @return Klase akcji zapisu do bazy danych.
	 */
	public SaveSqlAction getSaveSqlAction(){
		return new SaveSqlAction();

	}
	/**
	 * Metoda tworzy i zwraca klase obs�uguj�c� odczyt danych z bazy danych.
	 * @return Klase akcji odczytu z bazy danych.
	 */
	public LoadSqlAction getLoadSqlAction(){
		return new LoadSqlAction();
	}

	/**
	 * Metoda tworzy i zwraca klase obs�uguj�ca wydarzenie wy�wietlaj�ce okienko "o programie".
	 * @return klase obs�uguj�ca wydarzenie wy�wietlaj�ce okienko "o programie".
	 */
	public About getAboutAction(){
		return new About();
	}
	/**
	 *  Metoda tworzy i zwraca klase obs�uguj�ca wydarzenie wy�wietlaj�ce okienko w kt�rym podajemy dat� usuni�cia wydarze� starszych ni� podana data.
	 * @return Klasa REmoveOlderThan.
	 */
	public RemoveOlderThan getRemoveOlderThanAction(){
		return new RemoveOlderThan();
	}
	/**
	 * Metoda tworzy i zwraca klase obs�uguj�c� akcj� wyj�cia z programu.
	 */
	public QuitAction getQuitAction(){
		return new QuitAction();
	}
	/**
	 * Metoda od�wie�a widok okna g��wnego aplikacji.
	 */
	public void refresh(){
		this.mfv.rysujMiesiac();
		this.mfv.refreshView();
	}
	/**
	 *
	 */
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub

	}
	/**
	 * Metoda tworzy okno potwierdzaj�ce wykonan� operacj�.
	 * @param message wiadomosc do wy�wietlenia.
	 * @param title tytu� wiadomo�ci.
	 */
	public void okFrame(String message,String title){
		Object[] options = {"OK"};
		JOptionPane.showOptionDialog(new JFrame(),
				message,title,
				JOptionPane.PLAIN_MESSAGE,
				JOptionPane.QUESTION_MESSAGE,
				null,
				options,
				options[0]);
	}


	/**
	 * Zapis do bazy danych wydarzenia z alarmem.
	 * @param i indeks wydarzenia w tablicy wydarze�.
	 * @param stmt argument Statement
	 */
	public void toSqlWithAlarm(int i, Statement stmt){
		String dzien = "'"+EventsList.getArray().get(i).getDate().toString()+"'";
		String godzina = "'"+EventsList.getArray().get(i).getTime().toString()+"'";
		String tytul = "'"+EventsList.getArray().get(i).getTitle()+"'";
		String opis = "'"+EventsList.getArray().get(i).getNote()+"'";
		String dzienAlarmu = "'"+EventsList.getArray().get(i).getAlarmDate().toString()+"'";
		String czasAlarmu = "'"+EventsList.getArray().get(i).getAlarmTime().toString()+"'";
		String query = "INSERT INTO Events (dzien,godzina,tytul,opis,dzienAlarm,czasAlarm) values ("
				+dzien+","+godzina+","+tytul+","+opis+","+dzienAlarmu+","+czasAlarmu+")";

		try {
			stmt.execute(query);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println(query);
	}

	/**
	 * Zapis do bazy danych wydarzenia bez alarmu.
	 * @param i indeks wydarzenia w tablicy wydarze�.
	 * @param stmt argument Statement
	 */
	public void toSqlWithoutAlarm(int i, Statement stmt){

		String dzien = "'"+EventsList.getArray().get(i).getDate().toString()+"'";
		String godzina = "'"+EventsList.getArray().get(i).getTime().toString()+"'";
		String tytul = "'"+EventsList.getArray().get(i).getTitle()+"'";
		String opis = "'"+EventsList.getArray().get(i).getNote()+"'";
		String query = "INSERT INTO Events (dzien,godzina,tytul,opis) values ("
				+dzien+","+godzina+","+tytul+","+opis+")";

		try {
			stmt.execute(query);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println(query);

	}

}

