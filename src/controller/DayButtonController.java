package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;

import model.EventsFrameModel;
import view.DayButtonView;
import view.EventsFrameView;
import view.MainFrameView;

/**
 * Klasa przycisku tworzaca nowa ramke obslugi zdarzen danego dnia.
 *
 */
public class DayButtonController implements ActionListener{

	/**
	 * Zmienna przechowuje widok okna g��wnego aplikacji
	 */
	private MainFrameView mainFrameView;
	/**
	 * Zmienna przechowuje widok przycisku danego dnia.
	 */
	private DayButtonView dayButtonView;

	/**
	 * Konstruktor inicjalizujacy zmienna mainFrameView.
	 * @param mfv - MainFrameView
	 */
	public DayButtonController(DayButtonView dbv, MainFrameView mfv){
		this.mainFrameView=mfv;
		this.dayButtonView=dbv;
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		int d = dayButtonView.getDayButtonModel().getNrDnia();		//	pobiera dzien
		int m = dayButtonView.getDayButtonModel().getNrMiesiaca();	//	pobiera miesiac
		int y = dayButtonView.getDayButtonModel().getNrRoku();		//	pobiera rok

		LocalDate ld = LocalDate.of(y, m, d);
		EventsFrameModel fModel = new EventsFrameModel(ld);
		EventsFrameView f = new EventsFrameView(fModel,this.mainFrameView);	//	tworzy nowa ramke wydarzenia

	}
}
