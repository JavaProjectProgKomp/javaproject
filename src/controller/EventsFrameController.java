package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.EventsFrameView;
import view.NewEventFrameView;

/**
 * Klasa (kontroler) okna listy wydarze�.
 */
public class EventsFrameController implements ActionListener{

	/**
	 * Zmienna widoku okna wydarze�.
	 */
	EventsFrameView eventFrameView;

	/**
	 * Konstruktor inicjalizuj�cy widok okna listy wydarze�.
	 * @param v widok okna listy wydarze�.
	 */
	public EventsFrameController( EventsFrameView v){
		this.eventFrameView=v;
	}


	@Override
	/**
	 * Metoda ta, po kliknieciu w przycisk dodaj�cy nowe wydarzenie. Tworzy nowe okno dodawania wydarze�.
	 */
	public void actionPerformed(ActionEvent e) {

		NewEventFrameView nefv = new NewEventFrameView(eventFrameView.getModel().getEvent(),eventFrameView.getMainFrameView());
		nefv.refreshView();
		this.eventFrameView.closeView();
	}
}

