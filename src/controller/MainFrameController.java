package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.MainFrameModel;
import view.MainFrameView;
import view.MainFrameController.MonthAction;
import view.MainFrameController.YearAction;

/**
 * Klasa kontrolera g��wnego okna aplikacji.
 */
public class MainFrameController implements ActionListener{

	/**
	 * Zmienna modelu g��wnego okna aplikacji.
	 */
	private MainFrameModel mainFrameModel;
	/**
	 * Zmienna widoku g��wnego okna aplikacji.
	 */
	private MainFrameView mainFrameView;
	/**
	 * Konstruktor kontrolera g��wnego okna aplikacji.
	 * @param mainFrameModel model g��wnego okna aplikacji.
	 * @param mainFrameView widok g��wnego okna aplikacji.
	 */
	public MainFrameController(MainFrameModel mainFrameModel, MainFrameView mainFrameView){

		this.mainFrameModel = mainFrameModel;
		this.mainFrameView = mainFrameView;
	}
	/**
	 * 	Klasa wewn�trzna dla akcji zmiany miesiecy przy wyborze z JComboBox.
	 */
	private class MonthAction implements ActionListener{

		@Override
		/**
		 * Metoda rysuje przyciski dni wybranego miesi�ca z rozwijanej listy.
		 */
		public void actionPerformed(ActionEvent e) {
			if(mainFrameView.getWyborMiesiaca().getSelectedItem()=="Styczen"){
				mainFrameModel.setNrMiesiaca(1);
				mainFrameView.rysujMiesiac();
			}
			if(mainFrameView.getWyborMiesiaca().getSelectedItem()=="Luty"){
				mainFrameModel.setNrMiesiaca(2);
				mainFrameView.rysujMiesiac();
			}
			if(mainFrameView.getWyborMiesiaca().getSelectedItem()=="Marzec"){
				mainFrameModel.setNrMiesiaca(3);
				mainFrameView.rysujMiesiac();
			}
			if(mainFrameView.getWyborMiesiaca().getSelectedItem()=="Kwiecien"){
				mainFrameModel.setNrMiesiaca(4);
				mainFrameView.rysujMiesiac();
			}
			if(mainFrameView.getWyborMiesiaca().getSelectedItem()=="Maj"){
				mainFrameModel.setNrMiesiaca(5);
				mainFrameView.rysujMiesiac();
			}
			if(mainFrameView.getWyborMiesiaca().getSelectedItem()=="Czerwiec"){
				mainFrameModel.setNrMiesiaca(6);
				mainFrameView.rysujMiesiac();
			}
			if(mainFrameView.getWyborMiesiaca().getSelectedItem()=="Lipiec"){
				mainFrameModel.setNrMiesiaca(7);
				mainFrameView.rysujMiesiac();
			}
			if(mainFrameView.getWyborMiesiaca().getSelectedItem()=="Sierpien"){
				mainFrameModel.setNrMiesiaca(8);
				mainFrameView.rysujMiesiac();
			}
			if(mainFrameView.getWyborMiesiaca().getSelectedItem()=="Wrzesien"){
				mainFrameModel.setNrMiesiaca(9);
				mainFrameView.rysujMiesiac();
			}
			if(mainFrameView.getWyborMiesiaca().getSelectedItem()=="Pazdziernik"){
				mainFrameModel.setNrMiesiaca(10);
				mainFrameView.rysujMiesiac();
			}
			if(mainFrameView.getWyborMiesiaca().getSelectedItem()=="Listopad"){
				mainFrameModel.setNrMiesiaca(11);
				mainFrameView.rysujMiesiac();
			}
			if(mainFrameView.getWyborMiesiaca().getSelectedItem()=="Grudzien"){
				mainFrameModel.setNrMiesiaca(12);
				mainFrameView.rysujMiesiac();
			}
		}
	}

	/**
	 * 	Klasa wewn�trzna zmiany akcji przy wyborze roku z JComboBoxa
	 *
	 */
	class YearAction implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			int rok = Integer.parseInt( mainFrameView.getSelectedYear().toString() );
			mainFrameModel.setNrRoku(rok);
			mainFrameView.rysujMiesiac();
		}
	}
	/**
	 * Metoda tworzy i zwraca klas� akcji wyboru miesi�ca.
	 * @return klas� akcji wyboru miesi�ca..
	 */
	public MonthAction getMonthAction(){
		return new MonthAction();
	}
	/**
	 * Metoda tworzy i zwraca klas� akcji wyboru miesi�ca.
	 * @return zwraca klas� akcji wyboru miesi�ca.
	 */
	public YearAction getYearAction(){
		return new YearAction();
	}

	@Override
	/**
	 *
	 */
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

	}
	/**
	 * Metoda zwraca model g��wnego okna aplikacji
	 * @return
	 */
	public MainFrameModel getMainFrameModel(){
		return this.mainFrameModel;
	}


}
