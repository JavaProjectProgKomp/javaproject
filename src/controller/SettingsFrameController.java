package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import view.SettingsFrameView;

/**
 * Klasa kontrolera okna ustawie� aplikacji.
 */
public class SettingsFrameController implements ActionListener{

	/**
	 * Zmienna widoku okna ustawie�.
	 */
	private SettingsFrameView sfv;

	/**
	 * Konstruktor inicjalizuje widok okna ustawie�.
	 * @param sfv widok okna ustawie�.
	 */
	public SettingsFrameController(SettingsFrameView sfv){
		this.sfv=sfv;
	}

	@Override
	/**
	 * Metoda ta tworzy nowy widok okna ustawie�.
	 */
	public void actionPerformed(ActionEvent e) {
		new SettingsFrameView();
	}

	/**
	 * Klasa wewn�trzna obs�uguj�ca RadioButtony z nazwami motyw�w graficznych aplikacji.
	 *
	 */
	class RadioButtonsAction implements ActionListener{

		@Override
		/**
		 * Metoda ustawia motyw graficzny aplikacji.
		 */
		public void actionPerformed(ActionEvent e) {

			sfv.getModel().setTheme(e.getActionCommand());
			//System.out.println(sfv.getModel().getTheme());
		}
	}
	/**
	 *  Klasa wewn�trzna obs�uguj�ca przycisk potwierdzaj�cy - "OK".
	 */
	class OkButtonAction implements ActionListener{

		@Override
		/**
		 * Metoda akcji zapisuj�ca ustawienia do pliku config.cfg po klikni�ciu przycisku "OK", po czym zamyka okno ustawie�.
		 */
		public void actionPerformed(ActionEvent e) {
			try {

				//System.out.println(sfv.getModel().getTheme());
				BufferedWriter bw = new BufferedWriter(new FileWriter(new File("config.cfg")));
				bw.write(sfv.getModel().getTheme());
				bw.newLine();
				bw.close();
			} catch (IOException ex) {
				// TODO Auto-generated catch block
				ex.printStackTrace();
			}

			sfv.dispose();
		}
	}
	/**
	 * Metoda tworzy i zwraca obiekt klasy RadioButtonAction.
	 * @return obiekt klasy RadioButtonAction.
	 */
	public RadioButtonsAction getRadioButtonsAction(){
		return new RadioButtonsAction();
	}
	/**
	 * Metoda tworzy i zwraca obiekt klasy OkButtonAction.
	 * @return obiekt klasy OkButtonAction.
	 */
	public OkButtonAction getOkButtonAction(){
		return new OkButtonAction();
	}

}