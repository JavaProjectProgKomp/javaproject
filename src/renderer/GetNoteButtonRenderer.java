
package renderer;

import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.UIManager;

import javax.swing.table.TableCellRenderer;

import java.awt.Component;



// Klasy dodajace do tabeli wydarzen przyciski dodajace/ustawiajace alarm
/**
 * Klasa renderująca przycisk pokazujacy opis wydarzenia
 */
public class GetNoteButtonRenderer extends JButton implements TableCellRenderer {

	public GetNoteButtonRenderer() {
		setOpaque(true);

	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		if (isSelected) {
			setForeground(table.getSelectionForeground());
			setBackground(table.getSelectionBackground());
		} else {
			setForeground(table.getForeground());
			setBackground(UIManager.getColor("Button.background"));
		}
		setText((value == null) ? "" : value.toString());
		return this;
	}
}

