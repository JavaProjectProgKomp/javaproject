### DO ZROBIENIA:

- opcje
--
- konwersje zdarzen do wybranego formatu standardowego (np. Outlook, Evolution) - (ical4j)
- mnemoniki oraz skróty
- poprawic watki (sprawdzajce czas alarmow i starych wydarzen)


### ZROBIONO:
- watek kasujacy wydarzenia starsze niz... (zamieniono watek na opcje)
- sformatowanie komorek w tabeli wydarzen (zamieniono na dodanie przycisku wyswietlajacego opis)
- o programie
- zapis/odczyt BD
- ustawienie alarmu
- usuwanie wydarzen
- dodawanie wydarzen
- funkcja rysujRamke() - (odswiezanie ramki)
- funkcje wyboru miesiaca i roku
- klasa wydarzen
- kontenerowa klasa wydarzen
- akcje przyciskow dni tygodnia (okienko "Dodaj wydarzenie, wyswietlenie wydarzen, ustaw alarm")
- okienko z formularzem na podanie tytulu, godziny oraz notatki
- podswietlenie przycisku dnia, ktory zawiera jakies ustawione wydarzenie
- sortowanie tabeli wydarzen
- zapis/odczyt XML



## Programowanie komponentowe
## Tematyka i zakres projektu 
Projekt polega na napisaniu aplikacji w Javie z graficznym interfejsem uzytkownika na bazie
pakietu javax.swing. Aplikacja musi byc zaprojektowana w modelu trójwarstwowym.
Aplikacja posiada funkcjonalnosc prostego kalendarza „organizera” (jak np. Outlook, Evolu- 
tion, itp.), w szczególnosci umozliwia:

1. zapisywanie terminów zdarzen, krótkich ich opisów i miejsc,
2. ustawianie przypomnien/alarmów dzwiekowych na wybrany czas przed zdarzeniem,
3. korzystanie z graficznego kalendarza (np. przypominajacego kalendarz scienny)
4. konwersje zdarzen do wybranego formatu standardowego (np. Outlook, Evolution).
5. kasowanie zdarzen starszych niz (wybrana data), filtrowanie zdarzen,
6. zapis i odczyt zdarzen/spotkan do/z bazy danych i do/z formatu XML,
7. wyswietlanie okienka dialogowego typu „Ustawienia” w którym uzyte sa komponenty z
Tydzien 09 i Tydzien 10,
8. wyswietlanie okienko dialogowego „O programie”.
Wszelkie inne dodatkowe elementy (np. lista kontaktów, powiazania zdarzen z kontaktami, listy zadan, itp.), moga podwyzszac ocene pod warunkiem uzgodnienia tego z prowadzacym PRZED rozpoczeciem projektu.
Funkcjonalnosc
1. Aplikacja obsługuje podstawowe zachowania okienka (np. wył ˛aczanie krzy˙zykiem lub
Alt+F4, menu systemowe).
2. Aplikacja posiada menu główne, a w nim opcje do odczytu/zapisu zdarzen i ustawien z/do
bazy danych, itp.
3. Obsługa aplikacji przy pomocy myszy oraz klawiatury.
4. Aplikacja posiada wszystkie elementy i fragmenty kodu, które były przedmiotem cwiczen
laboratoryjnych (np. obsługa zdarzen we wszystkich wariantach, interfejsy, wyjatki, argumenty wywołania programu, itd.)